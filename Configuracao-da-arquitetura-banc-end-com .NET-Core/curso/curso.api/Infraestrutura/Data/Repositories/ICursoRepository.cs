﻿using curso.api.Business.Entities;
using System.Collections.Generic;

namespace curso.api.Infraestrutura.Data.Repositories
{
    public interface ICursoRepository
    {
        void Adicionar(Curso curso);
        void Commit();
        IList<Curso> ObterPorUsuario(int CodigoUsuario);


    }
}
